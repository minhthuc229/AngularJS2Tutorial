import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {TutorialComponent} from './tutorial.component';
import { AppComponent }  from './app.component';
import {FormsModule} from "@angular/forms";
@NgModule({
  imports:      [ BrowserModule,FormsModule],
  declarations: [ AppComponent, TutorialComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
