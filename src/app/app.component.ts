import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  template: '<h1>Hello {{name}}</h1>'
            +'<my-tutorial> hi</my-tutorial>' +
  '<img [src]="image" alt="image">' +
  '<button (click)="onClick()">click me</button>'+
  // '<button (mouseover)="onClick(val.value)">click me</button>' +
  '<input type="text" #val>' +
  '<p>{{name.value}}</p>' +
  '<input type="text" [(ngModel)]="fname"/>' +
  '<input type="text" [(ngModel)]="lname"/>' +
  '<br>' +
  '<p>{{fname}} {{lname}}</p>',
  styles: ['h2{color: red}']
})
export class AppComponent  {
  public name = "vietnam";
  public value = 'vietnam';
  public image = "http://lorempixel.com/800/300";
  public onClick(){
    console.log("hello");
  }
}
