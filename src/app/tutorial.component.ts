/**
 * Created by minht on 5/28/2017.
 */
import {Component} from '@angular/core';
@Component({
    selector:'my-tutorial',
    template:'<h2>This is the first Agularjs for 2, that version 2 is so anwsome</h2>' +
    '<h3 [style.color]="blue">hello</h3>',
    styles:['h2{color: blue}']
})
export class TutorialComponent{
    public blue = "red";
    public blueColor(){
        if (this.blue){
            return "blue";
        }else return "white";
    }
}